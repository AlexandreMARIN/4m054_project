# -*- coding: utf-8 -*-

import numpy as np
import scipy.sparse.linalg
import scipy.sparse
import matplotlib.pyplot as plt
import matplotlib.tri as mtri#pour les triangulations
from mpl_toolkits.mplot3d import Axes3D#pour les dessins en 3D


################################
#for extracting data from meshes
################################

def get_edge_from_nodes(Xnb, Ynb):
    """
    Input : integers representing nodes
    returns an np.array whose values are sorted
    """
    if(Xnb<Ynb):
        return np.array([Xnb, Ynb], dtype=np.int32)
    else:
        return np.array([Ynb, Xnb], dtype=np.int32)


def get_edges_from_triangle(Xnb, Ynb, Znb):
    """
    Input : three integers representing nodes which define a triangle
    returns the edges of the triangle
    """
    e1 = get_edge_from_nodes(Xnb, Ynb)
    e2 = get_edge_from_nodes(Xnb, Znb)
    e3 = get_edge_from_nodes(Ynb, Znb)
    return e1, e2, e3

def get_mesh_from_file(filename):
    """
    Input : name of the file which contains the mesh
    returns :
    - nodes, a np.array of floats
    - tri, a (triangles number * 3)-np.array of integers
    - boundary, a (number of edges on the boundary * 2)-np.array of integers which contains the edges on the boundary of the mesh
    - h := max{diam(t), t in the set of triangles}
    """
    f = open(filename)
    f.readline()
    nodesnb = int(f.readline())
    nodes = np.empty((nodesnb, 3))
    f.readline()
    f.readline()
    for i in range(nodesnb):
        aux = f.readline().split("\t")
        nodes[i][0] = float(aux[0])
        nodes[i][1] = float(aux[1])
        if(len(aux)==3):
            nodes[i][2] = float(aux[2])
        else:
            nodes[i][2] = 0.0

    f.readline()
    f.readline()
    trinb = int(f.readline())#triangle number
    f.readline()
    f.readline()
    tri = np.empty((trinb, 3), dtype=np.int32)
    bound_edges = set()#it will contain all the edges on the boundary
    h = 0.0#finesse du maillage

    for i in range(trinb):
        aux = f.readline().split("\t")
        tri[i][0] = int(aux[0])
        tri[i][1] = int(aux[1])
        tri[i][2] = int(aux[2])

        e1, e2, e3 = get_edges_from_triangle(tri[i][0], tri[i][1], tri[i][2])
        for e in [e1, e2, e3]:
            e_len = np.sqrt((nodes[e[0], 0]-nodes[e[1], 0])**2 + (nodes[e[0], 1]-nodes[e[1], 1])**2)
            if e_len > h:
                h = e_len
            if (e[0], e[1]) in bound_edges:
                bound_edges.remove((e[0], e[1]))
            else:
                bound_edges.add((e[0], e[1]))

    boundary = np.empty((len(bound_edges), 2), dtype=np.int32)
    i = 0
    for e in bound_edges:
        boundary[i] = e
        i += 1
    f.close()
    return nodes, tri, boundary, h


#############
#for plotting
#############
def plot_boundary(nodes, boundary):
    """
    Input :
    - nodes, boundary are return values of get_mesh_from_file()
    It plots the boundary of the mesh.
    """
    for b in boundary:
        plt.plot([nodes[b[0], 0], nodes[b[1], 0]], [nodes[b[0], 1], nodes[b[1], 1]], 'r--', linewidth=1.5)


def plot_mesh(nodes, tri):
    """
    Input :
    - nodes, tri are return values of get_mesh_from_file()
    It plots the mesh.
    """
    x = nodes[:, 0]
    y = nodes[:, 1]
    triang = mtri.Triangulation(x, y, tri)
    plt.triplot(triang)

def show_mesh():
    """
    It shows the mesh you want to see (triangles + boundary).
    """
    meshnb = input("Give an integer (give -1 for all meshes): ")
    if not meshnb in {"1", "2", "3"}:
        meshnb = ["1", "2", "3"]
        for nb in meshnb:
            nodes, tri, boundary, h = get_mesh_from_file("maillage"+nb+"_5.txt")
            plot_boundary(nodes, boundary)
            plot_mesh(nodes, tri)
            X = nodes[:, 0]
            plt.xlim([min(X)-0.1, max(X)+0.1])
            Y = nodes[:, 1]
            plt.ylim([min(Y)-0.1, max(Y)+0.1])
            plt.xlabel("$x$", fontsize="x-large")
            plt.ylabel("$y$", fontsize="x-large")
            plt.title("maillage "+nb, fontsize="x-large")
            plt.show()
    else:
        nodes, tri, boundary, h = get_mesh_from_file("maillage"+meshnb+"_5.txt")
        plot_boundary(nodes, boundary)
        plot_mesh(nodes, tri)
        X = nodes[:, 0]
        plt.xlim([min(X)-0.1, max(X)+0.1])
        Y = nodes[:, 1]
        plt.ylim([min(Y)-0.1, max(Y)+0.1])
        plt.xlabel("$x$", fontsize="x-large")
        plt.ylabel("$y$", fontsize="x-large")
        plt.title("maillage "+meshnb, fontsize="x-large")
        plt.show()

################################
#for building the linear system
################################

def MassElem(s1, s2, s3):
    """
    Matrice de masse pour un triangle
    """
    M_T = np.array([[1.0/6, 1.0/12, 1.0/12], [1.0/12, 1.0/6, 1.0/12], [1.0/12, 1.0/12, 1.0/6]])
    area = 0.5*abs( (s1[0]-s2[0])*(s2[1]-s3[1]) - (s1[1]-s2[1])*(s2[0]-s3[0]) )

    M_T *= area
    return M_T


def RigElem(s1, s2, s3):
    """
    Matrice de rigidité pour un triangle
    """
    K_T = np.empty( (3, 3) )
    area = 0.5*abs( (s1[0]-s2[0])*(s2[1]-s3[1]) - (s1[1]-s2[1])*(s2[0]-s3[0]) )
    vert = np.array([np.array(s1), np.array(s2), np.array(s3)])
    for i in range(3):
        for j in range(3):
            K_T[i, j] = np.transpose((vert[(i+2)%3]-vert[(i+1)%3])).dot((vert[(j+2)%3]-vert[(j+1)%3]))
    K_T /= 4.0*area
    return K_T

def B_Elem(s1, s2):
    """
    s1, s2 : extrémités d'une arête du bord
    renvoie la matrice élémentaire pour une arête du bord, voir l'équation (4)
    """
    B_g = np.array([[1.0/3.0, 1.0/6.0], [1.0/6.0, 1.0/3.0]])
    length = np.linalg.norm(s1-s2)
    B_g *= length
    return B_g

def build_M(nodes, triangles):
    """
    Matrice de masse
    """
    M = np.zeros( (nodes.shape[0], nodes.shape[0]) )
    for q in range(triangles.shape[0]):
        aux = MassElem(nodes[triangles[q, 0]], nodes[triangles[q, 1]], nodes[triangles[q, 2]])
        for l in range(3):
            for m in range(3):
                M[triangles[q, m], triangles[q, l]] += aux[l, m]

    return M


def build_K(nodes, triangles):
    """
    Matrice de rigidité du système
    """
    K = np.zeros( (nodes.shape[0], nodes.shape[0]) )
    for q in range(triangles.shape[0]):
        aux = RigElem(nodes[triangles[q, 0]], nodes[triangles[q, 1]], nodes[triangles[q, 2]])
        for l in range(3):
            for m in range(3):
                K[triangles[q, m], triangles[q, l]] += aux[l, m]

    return K


def build_F(M, nodes, f):
    """
    construit le second membre
    M doit être la matrice de masse
    """
    vec = np.empty( (nodes.shape[0]) )
    for i in range(nodes.shape[0]):
        vec[i] = f(nodes[i][0], nodes[i][1])
    return M.dot(vec)

def build_B(nodes, triangles, boundary):
    """
    Matrice B de la formulation (4)
    """
    B = np.zeros( (triangles.shape[0], boundary.shape[0]) )
    for q in range(boundary.shape[0]):
        aux = B_Elem(nodes[boundary[q, 0]], nodes[boundary[q, 1]])
        for l in range(2):
            for m in range(2):
                B[boundary[q, m], boundary[q, l]] += aux[m, l]

    return B

def build_G(nodes, boundary, g):
    """
    Vecteur G de la formulation (4)
    """
    G = np.zeros(boundary.shape[0])
    for i in range(boundary.shape[0]):
        n0 = nodes[boundary[i, 0]]
        n1 = nodes[boundary[i, 1]]
        g_mid = g(0.5*(n0[0]+n1[0]), 0.5*(n0[1]+n1[1]))
        length = np.linalg.norm(n0-n1)
        G[boundary[i, 0]] += length*(g(n0[0], n0[1]) + 2.0*g_mid)/6.0
        G[boundary[i, 1]] += length*(g(n1[0], n1[1]) + 2.0*g_mid)/6.0
    return G

def pseudo_elimination(A, nodes0):
    """
    A : matrice du système
    nodes0 : indices des noeuds sur le bord
    Réalise la pseudo-élimination sur A à partir de nodes0
    """
    for n in nodes0:
        for i in range(n):
            A[i, n] = 0.0
            A[n, i] = 0.0

        A[n, n] = 1.0

        for i in range(n+1, A.shape[0]):
            A[i, n] = 0.0
            A[n, i] = 0.0
    return A


#################
#norms and errors
#################

def norm_H1(u, A):
    """
    u : interpolation de la solution du problème continu
    A = M+K : matrice de masse + matrice de rigidité

    renvoie la norme H^1 de u
    """
    return np.sqrt(((u.reshape((1, u.shape[0]))).dot(A)).dot(u))

def error_H1(u, u_h, A):
    """
    u : interpolation de la solution du problème continu
    u_h : coordonnées dans la base des fonctions de forme de la solution au problème discret
    A = M+K : matrice de masse + matrice de rigidité

    renvoie la norme H^1 de l'erreur u-u_h
    """
    return norm_H1(u-u_h, A)

def error_L2(u, u_h, M):
    """
    u : interpolation de la solution du problème continu
    u_h : coordonnées dans la base des fonctions de forme de la solution au problème discret
    M : matrice de masse

    renvoie la norme L2 de l'erreur u-u_h
    """
    return np.sqrt((((u-u_h).reshape((1, u.shape[0]))).dot(M)).dot(u-u_h))

def show_errors(meshnumber, u, f, title):
    """
    Affiche les erreurs en fonction de la finesse du maillage, lorsque g est nulle.
    """
    h_ = np.empty(5)
    errL2 = np.empty(5)
    errH1 = np.empty(5)
    for i in range(1, 6):
        #we build the linear system
        nodes, tri, boundary, h = get_mesh_from_file("maillage"+meshnumber+"_"+str(i)+".txt")
        h_[i-1] = h
        M = build_M(nodes, tri)
        K = build_K(nodes, tri)
        F = build_F(M, nodes, f)

        #nodes on the boundary
        nodes0 = set()
        for b in boundary:
            nodes0.add(int(b[0]))
            nodes0.add(int(b[1]))

        A = pseudo_elimination(K, nodes0)
        A = scipy.sparse.csr_matrix(A)
        for n in nodes0:
            F[n] = 0.0
        U, info = scipy.sparse.linalg.cg(A, F)
        U_exact = np.empty(U.shape[0])
        for k in range(U.shape[0]):
            U_exact[k] = u(nodes[k, 0], nodes[k, 1])
        errL2[i-1] = error_L2(U_exact, U, M)
        errH1[i-1] = error_H1(U_exact, U, M+K)
    plt.plot(h_, errL2)
    plt.plot(h_, errH1)
    plt.plot(h_, h_)
    plt.plot(h_, h_**2)
    plt.title(title, fontsize="x-large")
    plt.legend([r"$||u-u_{h}||_{L^2}$", r"$||u-u_{h}||_{H^1}$", r"$h$", r"$h^2$"], loc="lower right")
    plt.xlabel(r"$h$", fontsize="x-large")
    plt.ylabel("erreur", fontsize="x-large")
    plt.xscale("log")
    plt.yscale("log")
    plt.show()

###############################
#for solving
###############################

def solve_homog_Dirichlet(mesh, f):
    """
    résout le problème (1) lorsque g=0
    mesh : fichier de maillage
    f : fonction modélisant f dans le problème (1)
    Renvoie :
    - nodes, tri, boundary, h : données du maillage
    - U, M, K, F : solution, matrices de masse et de rigidité, vecteur représentant la forme linéaire du problème discret
    """
    #we load data
    nodes, tri, boundary, h = get_mesh_from_file(mesh)

    #then we build the linear system
    M = build_M(nodes, tri)
    K = build_K(nodes, tri)
    F = build_F(M, nodes, f)

    #nodes on the boundary
    nodes0 = set()
    for b in boundary:
        nodes0.add(int(b[0]))
        nodes0.add(int(b[1]))

    A = pseudo_elimination(K, nodes0)
    A = scipy.sparse.csr_matrix(A)
    for n in nodes0:
        F[n] = 0.0
    U, info = scipy.sparse.linalg.cg(A, F)
    return (nodes, tri, boundary, h, U, M, K, F)

def solve_Dirichlet_by_pseudo_elimination(mesh):
    """
    résout le problème (1) lorsque g(x)=x^2/4 et f vaut 0
    mesh : fichier de maillage
    Renvoie :
    - nodes, tri, boundary, h : données du maillage
    - U, M, K, F : solution, matrices de masse et de rigidité, vecteur représentant la forme linéaire du problème discret
    """
    def f(x, y):
        return 1.0
    def g(x, y):
        return 0.25*(x*x + y*y)
    #we load data
    nodes, tri, boundary, h = get_mesh_from_file(mesh)

    #then we build the linear system
    M = build_M(nodes, tri)
    K = build_K(nodes, tri)
    F = build_F(M, nodes, f)

    #nodes on the boundary
    nodes0 = set()
    for b in boundary:
        nodes0.add(int(b[0]))
        nodes0.add(int(b[1]))

    #pseudo-elimination
    A = pseudo_elimination(K, nodes0)
    A = scipy.sparse.csr_matrix(A)
    for n in nodes0:
        F[n] = 0.0
    U, info = scipy.sparse.linalg.cg(A, F)

    #we apply the remark
    for i in range(U.shape[0]):
        U[i] += g(nodes[i, 0], nodes[i, 1])

    return (nodes, tri, boundary, h, U, M, K, F)

def solve_Lagrange_mult(mesh, g):
    """
    résout le problème (1) lorsque f est nulle, grâce à la formulation (4)
    mesh : fichier de maillage
    g : fonction modélisant g dans le problème (1)
    Renvoie :
    - f
    - nodes, tri, boundary, h : données du maillage
    - nodes0, nodes_int : ensembles contenant respectivement les indices des noeuds du bord et ceux de l'intérieur
    - U0 : coordonnées de U associées aux noeuds du bord (les indices sont rangés par ordre croissant)
    - U_int : coordonnées de U associées aux noeuds intérieurs (les indices sont rangés par ordre croissant)
    - L
    """
    #we load data
    nodes, tri, boundary, h = get_mesh_from_file(mesh)

    #we build the linear system
    A = build_K(nodes, tri)
    B = build_B(nodes, tri, boundary)
    G = build_G(nodes, boundary, g)

    #nodes on the boundary
    nodes0 = set()
    for e in boundary:
        nodes0.add(e[0])
        nodes0.add(e[1])

    B_ = scipy.sparse.csr_matrix(B[sorted(nodes0)])
    U0, info = scipy.sparse.linalg.cg(B_.transpose(), G)

    #nodes inside the domain
    nodes_int = set(range(nodes.shape[0])) - nodes0
    A_ = A[sorted(nodes_int)]
    A_int = scipy.sparse.csr_matrix(A_[0:, sorted(nodes_int)])
    A_0 = scipy.sparse.csr_matrix(A_[0:, sorted(nodes0)])
    U_int, info = scipy.sparse.linalg.cg(A_int, -A_0*U0)

    A_ = A[sorted(nodes0)]
    A_int = scipy.sparse.csr_matrix(A_[0:, sorted(nodes_int)])
    A_0 = scipy.sparse.csr_matrix(A_[0:, sorted(nodes0)])
    L, info = scipy.sparse.linalg.cg(B_, -(A_0*U0+A_int*U_int))

    return nodes, tri, boundary, h, nodes0, nodes_int, U0, U_int, L

################################
#réponses directes aux questions
################################
def question1():
    show_mesh()

def question4():
    def f(x, y):
        return 1.0
    def u(x, y):
        return 0.25*(1.0-x**2-y**2)
    nodes, tri, boundary, h, U, M, K, F = solve_homog_Dirichlet("maillage1_1.txt", f)
    X = nodes[:, 0]
    Y = nodes[:, 1]
    #reconstruction de la solution à l'aide des triangulations
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_trisurf(X, Y, U, triangles=tri, linewidth=0.2, antialiased=True)
    plt.title(r"approximation de $u(x,\, y)=\frac{1}{4}(1\,-\,x^{2}-\,y^{2})$", fontsize="xx-large")
    plt.show()
    show_errors("1", u, f, r"erreurs pour le maillage 1, $f=1,\ g=0$")

def question5():
    """
    On affiche une carte de couleur pour u_(h)^(1) et pour les trois géométries
    """
    for n in range(1, 4):
        nodes, tri, boundary, h, U, M, K, F = solve_Dirichlet_by_pseudo_elimination("maillage"+str(n)+"_3.txt")
        plt.tripcolor(nodes[:, 0], nodes[:, 1], tri, U)
        plt.colorbar()
        plt.title("$u^{(1)}_{h}$ pour le maillage "+str(n), fontsize="x-large", position=(0.3, 1.02))
        plt.xlabel(r"$x$", fontsize="x-large")
        plt.ylabel(r"$y$", fontsize="x-large")
        plt.show()

def question6():
    h_ = np.empty(5)
    errH1 = np.empty(5)
    for i in range(1, 6):
        nodes, tri, boundary, h, U, M, K, F = solve_Dirichlet_by_pseudo_elimination("maillage1_"+str(i)+".txt")
        h_[i-1] = h
        U_exact = 0.25*np.ones((U.shape[0]))
        errH1[i-1] = error_H1(U_exact, U, M+K)/norm_H1(U_exact, M+K)

    plt.plot(h_, errH1)
    plt.plot(h_, h_)
    plt.plot(h_, h_**1.5)
    plt.title(r"erreurs relatives pour le maillage 1, $f\equiv0,\,g\equiv1/4$", fontsize="x-large")
    plt.legend([r"$E_{h}^{1}$", r"$h$", r"$h^{3/2}$"], loc="lower right")
    plt.xlabel(r"$h$", fontsize="x-large")
    plt.ylabel("erreur", fontsize="x-large")
    plt.xscale("log")
    plt.yscale("log")
    plt.show()

def question9():
    def g(x, y):
        return 0.25*(x*x+y*y)

    h_ = np.empty(5)
    errH1 = np.empty(5)

    for a in ["1", "2"]:
        for i in range(1, 6):
            nodes, tri, boundary, h, U1, M, K, F = solve_Dirichlet_by_pseudo_elimination("maillage"+a+"_"+str(i)+".txt")
            h_[i-1] = h
            nodes, tri, boundary, h, nodes0, nodes_int, U2_0, U2_int, L = solve_Lagrange_mult("maillage"+a+"_"+str(i)+".txt", g)
            U2 = np.empty(U1.shape[0])
            nodes0 = sorted(nodes0)
            for k in range(len(nodes0)):
                U2[nodes0[k]] = U2_0[k]
            nodes_int = sorted(nodes_int)
            for k in range(len(nodes_int)):
                U2[nodes_int[k]] = U2_int[k]
            A = M+K
            errH1[i-1] = error_H1(U1, U2, A)/norm_H1(U1, A)

        plt.plot(h_, errH1)
        plt.plot(h_, h_)
        plt.plot(h_, h_**1.5)
        plt.title(r"erreurs relatives pour le maillage "+a+", $f\equiv0,\,g(x)=|x|^{2}/4$", fontsize="x-large")
        plt.legend([r"$E_{h}^{2}$", r"$h$", r"$h^{3/2}$"], loc="lower right")
        plt.xlabel(r"$h$", fontsize="x-large")
        plt.ylabel("erreur", fontsize="x-large")
        plt.xscale("log")
        plt.yscale("log")
        plt.show()

####################
#exécution du script
####################

question1()
question4()
question5()
question6()
question9()
